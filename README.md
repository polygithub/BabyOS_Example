# BabyOS_Example

基于STM32F107，Master分支实现BabyOS Readme.md内容里**使用方法**部分的操作。

------

## 1.使用STM32CubeMX建立工程

配置时钟，开启SPI，开启串口。

## 2.添加子模块并更新

```C
git submodule add https://gitee.com/notrynohigh/BabyOS.git
git submodule update --init --recursive
```

下载BabyOS配置文件放入工程：<https://gitee.com/notrynohigh/BabyOS_Config>

下载BabyOS硬件抽象层文件放入工程（使用st_hal分支文件）：<https://gitee.com/notrynohigh/BabyOS_Hal>

## 3.添加文件

```C
BabyOS
├── bos
│   ├── algorithm               
│   ├── core					//全部添加
│   ├── drivers					//添加 b_drv_spiflash
│   ├── modules					//全部添加
│   ├── thirdparty				//添加SFUD
│   └── utils
├── build						
├── CMakeLists.txt
├── doc							
├── Examples					
│   └── qpn
├── LICENSE						
├── main.c						
└── README.md
BabyOS_Config					//添加全部
BabyOS_Hal						//添加 gpio, uart, spi相关文件
```

## 4.添加头文件

![](https://gitee.com/notrynohigh/BabyOS_Example/raw/master/doc/include.png)

## 5.增加系统定时器

```C
/**
* @brief This function handles System tick timer.
*/
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */
  bHalIncSysTick();
  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  HAL_SYSTICK_IRQHandler();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}
```

## 6.使能KV功能模块

![](https://gitee.com/notrynohigh/BabyOS_Example/raw/master/doc/config.png)



## 7.注册设备

```C
B_DEVICE_REG(SPIFLASH, bSPIFLASH_Driver[0], "flash")
```

## 8.添加硬件平台头文件

```C
#ifndef __B_HAL_H__
#define __B_HAL_H__

#ifdef __cplusplus
 extern "C" {
#endif

/*Includes ----------------------------------------------*/
#include "b_config.h" 
#include "stm32f1xx_hal.h"   //add the platform h file
```

## 9.添加SPI和GPIO硬件抽象层

## 10.使用kv功能模块

```C
  /* USER CODE BEGIN WHILE */
  bInit();
  bKV_Init(SPIFLASH, 0, 40960, 4096);
  uint8_t tmp = 0xaa;
  bKV_Set("bos", &tmp, 1);
  tmp = 0;
  bKV_Get("bos", &tmp);
  b_log("tmp:%x\r\n", tmp);
  while (1)
  {
    bExec();
  /* USER CODE END WHILE */
```



![](https://gitee.com/notrynohigh/BabyOS_Example/raw/master/doc/KV.png)

BabyOS master分支 <https://gitee.com/notrynohigh/BabyOS>

BabyOS教程更新会在公众号推送：

![](https://gitee.com/notrynohigh/BabyOS_Example/raw/master/doc/QRcode.jpg)

